<?php

require_once("config.inc.php");
require_once("functions.inc.php");

global $user_id;

# Get today's date
$today = date("Y-m-d");

# If weekend
if (isWeekendOnDate($today)){
    die("Relax, it's your day off!");
}

/**
 * Get today's report
 */

$report = queryHarvest('api/v2/time_entries?user_id='.$user_id.'&from='.$today.'&to='.$today);

$reported_time = 0;

foreach ($report->time_entries as $key => $day) {
    # Sum hours
    $reported_time += $day->hours;
}

$time_reported_human_readable = decimalTimeToHumanReadable($reported_time);

if ($reported_time !== 0){
    echo "<strong>".$time_reported_human_readable['hours']."</strong> hours <strong>".$time_reported_human_readable['minutes']."</strong> minutes reported".PHP_EOL;
} else {
    echo '<strong class="alert">[ Nothing reported today ]</strong>'.PHP_EOL;
}