$(function () {

    // Get settings
    var hrvst = JSON.parse(localStorage.getItem("hrvst"));

    // Handle defaults
    if (hrvst === null) {
        hrvst = {
            'account_id': '',
            'user_id': '',
            'bearer': '',
            'favourites': {}
        };
    }

    // Function to show the toast
    function showToast(message, isRunning = false) {
        $(".toast")
            .text(message)
            .removeClass('hideVisibility');

        if (isRunning) {
            $(".toast").addClass('toastRunning');
        }

        // Automatically hide the toast after 3 seconds
        setTimeout(function () {
            hideToast();
        }, 3000);
    }

    // Function to hide the toast
    function hideToast() {
        $(".toast").addClass('hideVisibility').removeClass('toastRunning');
    }

    // Function to open the configuration window
    function openConfigWindow() {
        $('body')
            .append(
                $('<div class="config" id="auth">')
                    .append('<h2>Enter Hrvst stuff</h2>')
                    .append('<input type="text" id="account-id" placeholder="Harvest account Id" value="' + (hrvst.account_id || '') + '">')
                    .append('<input type="text" id="user-id" placeholder="Harvest user Id" value="' + (hrvst.user_id || '') + '">')
                    .append('<input type="text" id="bearer" placeholder="Bearer auth" value="' + (hrvst.bearer || '') + '">')
                    .append('<button id="set-auth">Set</button>')
            );
        
        $('#set-auth').on('click', function (e) {
            hrvst.account_id = $('#account-id').val();
            hrvst.user_id = $('#user-id').val();
            hrvst.bearer = $('#bearer').val();

            // Save data in local storage
            localStorage.setItem('hrvst', JSON.stringify(hrvst));

            showToast('Settings saved');

            // Empty thing
            $('#auth').remove();
        });
    }


    // If no account id is set or auth, show configuration window
    if (hrvst.account_id === '' || hrvst.auth) {
        openConfigWindow();
    }

    // Allow manual opening of config window via #editConfig
    $("#editConfig").on("click", function (e) {
        $('#auth').remove(); // Remove any existing config windows to avoid duplicates
        openConfigWindow();
    });

    // Render presets using the renderPresets function
    renderPresets(hrvst.favourites);

    // Start listener
    startPresetListener();

    // basic settings for requests
    var settings = {
        "url": "",
        "method": "",
        "timeout": 0,
        "headers": {
            "Harvest-Account-Id": hrvst.account_id,
            "Authorization": "Bearer " +hrvst.bearer,
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    };

    // Edit presets

    // Get last reports from user
    $("#editPresets").on("click", function (e) {

        // Show toast
        $(".harvest-reported").removeClass('hide');
        $(".content").addClass('hide');
        $('body')
            .append(
                $('<div class="config" id="auth">')
                    .append('<h2>Set Hrvst Presets</h2>')
                    .append('<textarea id="customPresets" >' + JSON.stringify(hrvst.favourites) + '</textarea>')
                    .append('<button id="setPresets">Save presets</button>')
            );

        $('#setPresets').on('click', function (e) {
            // Todo: Ask Ville about returning instead of using var
            hrvst.favourites = JSON.parse($('#customPresets').val());

            // Save data in local storage
            localStorage.setItem('hrvst',JSON.stringify(hrvst));

            showToast('Presets saved');

            // Empty thing
            $('#auth').remove();
            window.location.reload();
        });
    });

    // Fetch a users preset based on previous reported times
    $("#getPresets").on("click", function (e) {
        settings.url = 'https://api.harvestapp.com/api/v2/time_entries?user_id=' + hrvst.user_id + '&per_page=100';
        settings.method = 'GET';

        $.get(settings).done(function (response) {
            var projectsAndTasks = [];

            response.time_entries.forEach(time => {
                var entry = {
                    custom_title: time.client.name,
                    custom_task: time.task.name,
                    client: time.client.name,
                    project: time.project.name,
                    task: time.task.name,
                    project_id: time.project.id,
                    task_id: time.task.id,
                };

                // Check if the combination of project_id and task_id already exists in projectsAndTasks
                var exists = projectsAndTasks.some(item =>
                    item.project_id === time.project.id && item.task_id === time.task.id
                );

                if (!exists) {
                    projectsAndTasks.push(entry);
                }
            });

            // Render presets using the renderPresets function
            renderPresets(projectsAndTasks);

            hrvst.favourites = projectsAndTasks;
            localStorage.setItem('hrvst',JSON.stringify(hrvst));

            showToast('New presets fetched');
            // Start listener
            startPresetListener();
        });
    });

    function generatePresetHTML(entry) {
        return $(`<div data-project-id="${entry.project_id}" data-task-id="${entry.task_id}" />`)
            .append(`<span class="project">${entry.custom_title}</span>`)
            .append(`<span class="task">${entry.custom_task}</span>`);
    }

    function renderPresets(projectsAndTasks) {
        // Make sure to empty presets container
        $("#presets").empty();

        // Check if there are any existing presets
        if ($.isEmptyObject(projectsAndTasks)) {
          return;
        }

        projectsAndTasks.forEach(entry => {
            // Generate HTML and append to presets
            $("#presets").append(generatePresetHTML(entry));
        });
    }

    // Set time based on shared JSON time from a friendly colleague
    $("#customTime").on("click", function (e){
        let customSettings = settings;

        // Update with correct url and method
        customSettings.url = 'https://api.harvestapp.com/v2/time_entries';
        customSettings.method = 'POST';

        if ($("#customReport").val() !== '') {

            // Get JSON
            let json = JSON.parse($("#customReport").val());

            // Generate params
            let params = new URLSearchParams(json).toString();

            // update url
            customSettings.url = customSettings.url + '?' + params;

            $.post(customSettings).done(function (response) {
                $("#customReport").val('');
            });
        } else {
            alert("Jason is not here!");
        }
    });

    /**
     * Function that starts listener for project presets
     */
    function startPresetListener() {
        $("div.presets div").on("click", function (e) {
            // Get data from the preset clicked by userr
            let project_id = $(this).attr('data-project-id');
            let task_id = $(this).attr('data-task-id');
            let note = ($("#note").val() !== '')
                ? $("#note").val()
                : $(this).attr('data-note') ? $(this).attr('data-note') : '';
            let date = $("#date").val();
            let time = $("#quickTimeInput").val();

            let customSettings = settings;

            customSettings.url = "https://api.harvestapp.com/v2/time_entries?project_id=" + project_id + "&task_id=" + task_id + "&spent_date=" + date + "&notes=" + note + "&hours=" + (time / 60);
            customSettings.method = 'POST';

            // Todo: remove
            if (customSettings.headers['Harvest-Account-Id'] === '') {
                $('body').append('<div class="config"><div>Config UNFOUND! ☠️🔪💥🧨☣️</div></div>');
            }

            // Save
            $.post(customSettings).done(function (response) {
                // Empty note
                $("#note").val('');
                $("#quickTimeInput").val('');

                // Make sure nothing is active
                $(".quickTime div").removeClass('active');

                showToast('Entry saved');

                // If running timer set, start!
                if ($(".runningTimer").hasClass('active') && !response.is_running) {
                    // Start timer
                    customSettings.url = 'https://api.harvestapp.com/v2/time_entries/' + response.id + '/restart';
                    customSettings.method = 'PATCH';


                    // Do request to start timer
                    $.ajax(customSettings).done(function (response) {
                        showToast('Entry timer started');
                    }).fail(function (response) {
                        showToast('Unable to start timer');
                    });
                }

            }).fail(function (response) {
                showToast('Failed to save entry: ' + response.responseJSON.message);
            });

        });
    }

    // Get last reports from user
    $("#showReports").on("click", function (e) {
        // Use defaults and override
        let customSettings = settings;
        customSettings.url = 'https://api.harvestapp.com/api/v2/time_entries?user_id=' + hrvst.user_id + '&per_page=200';
        customSettings.method = 'GET';

        // Get things
        $.get(customSettings).done(function (response) {
            var projectsAndTasks = [];

            // Show toast
            $(".harvest-reported").removeClass('hide');
            $(".content").addClass('hide');

            response.time_entries.forEach(time => {
                var entry = {
                    client: time.client.name,
                    hours: time.hours,
                    notes: time.notes,
                    project: time.project.name,
                    task: time.task.name,
                    project_id: time.project.id,
                    task_id: time.task.id,
                    spent_date: time.spent_date
                };

                // Check if the combination of project_id and task_id already exists in projectsAndTasks
                var exists = projectsAndTasks.some(item =>
                    item.project_id === time.project.id && item.task_id === time.task.id
                );

                if (!exists) {
                    projectsAndTasks.push(entry);

                    $(".harvest-reported").append(
                        $("<div class='timeEntry' />")
                            .append(`<div>Client: ${time.client.name} <br>Project: ${time.project.name} </div>`)
                            .append(`<div>Task: ${time.task.name}<br>Note: ${time.notes}</div>`)
                            .append(`<div class="timeEntry__json"><textarea>{"project_id":"${time.project.id}","task_id":"${time.task.id}","notes":"${time.notes}" ,"hours":"${time.hours}","spent_date":"${time.spent_date}"}</textarea></div>`)
                    ); 
                }
            });
        });
    });

    // Click listener for quick time slot elements
    $("div.quickTime div").on("click", function (e) {
        // Why?
        if (e.target.id === "quickTimeInput") {
            return;
        }

        let timeSum = Number($("#quickTimeInput").val() ?? 0);

        let time = Number($(e.currentTarget).html());

        // If the user clicks any modifier use minus instead
        if (e.shiftKey || e.metaKey || e.ctrlKey) {
          $("#quickTimeInput").val(Number(timeSum - time));

        } else {
          $("#quickTimeInput").val(Number(time + timeSum));
        }
    })

    // Setting for allowing timer to run at entry start
    $("div.runningTimer").on("click", function (e) {
        $(this).toggleClass('active');
    });

    // Get current date and set
    let currentDate = new Date().toJSON().slice(0, 10);
    $("#date").val(currentDate);

     // Function to update the date input to today's date
    function updateDateToToday() {
        let currentDate = new Date().toISOString().slice(0, 10); // Get today's date in YYYY-MM-DD format
        let inputDate = $("#date").val();

        if (inputDate !== currentDate) {
            $("#date").val(currentDate); // Update the date input to today's date
        }
    }

    // Update date to todays date every hour to allow for quick custom history reports but 
    // not get stuck on reporting on previous days by mistake
    setInterval(updateDateToToday, 3600000);

});

