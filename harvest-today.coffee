# This is a simple example Widget, written in CoffeeScript, to get you started
# with Übersicht. For the full documentation please visit:
#
# https://github.com/felixhageloh/uebersicht
#
# You can modify this widget as you see fit, or simply delete this file to
# remove it.

# this is the shell command that gets executed every time this widget refreshes
command: "/opt/homebrew/bin/php ~/git/harvest/harvest-get-today.php"

# the refresh frequency in milliseconds
refreshFrequency: 36000

# render gets called after the shell command has executed. The command's output
# is passed in as a string. Whatever it returns will get rendered as HTML.
render: (output) -> """
<div>#{output}</div>
"""

# the CSS style for this widget, written using Stylus
# (http://learnboost.github.io/stylus/)
style: """
  z-index: 10
  background: rgba(#000, 0)
  font-family: Helvetica
  bottom: 120px
  left: 50px;
  div
    color: rgba(#ddd, 0.3)
    font-size:13px
  .alert
    color: rgba(200,0,0, 0.5)
    letter-spacing: 1px
    text-transform:uppercase
"""
