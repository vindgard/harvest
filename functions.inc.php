<?php

require_once("config.inc.php");

function stringOrDateTimeToFormatted(string | DateTime $date, string $format = 'Y-m-d'): string {
    if ($date instanceof DateTime) {
        return $date->format($format);
    }

    return date($format, strtotime($date));
}

function isHolidayOnDate(string | DateTime $date): bool {
    global $holidays;

    $yyyy_mm_dd = stringOrDateTimeToFormatted($date);

    return in_array($yyyy_mm_dd, $holidays, true);
}

function isWeekendOnDate(string | DateTime $date): bool {
    global $holidays;
    global $workdays_per_week;

    $yyyy_mm_dd = stringOrDateTimeToFormatted($date);
    $day_of_week = stringOrDateTimeToFormatted($date, 'N');

    if (in_array($yyyy_mm_dd, $holidays, true)) {
        return true;
    }

    return $day_of_week > $workdays_per_week;
}

function decimalTimeToHumanReadable($time): array {
    # Minutes in decimal
    $minutes_dec = fmod($time, 1);

    # Minutes in actual minutes
    $minutes = round($minutes_dec * 60);

    # Calculate hours
    $hours = $time - fmod($time,1);

    return ['hours' => $hours,'minutes' => $minutes];
}

function getWeekdaysRemainingThisMonth(): int {
    return getWeekdaysInMonthOnDate(new DateTime(), true);
}

function getHolidaysRemainingThisMonth(): int {
    return getHolidaysInMonthFromDate(new DateTime(), true);
}

function getHolidaysInMonthFromDate(string | DateTime $date, $onlyRemaining = false): int {
    global $holidays, $day_off_weekday;
    $yyyy_mm_dd = stringOrDateTimeToFormatted($date);
    $yyyy_mm = stringOrDateTimeToFormatted($date, 'Y-m');
    $holidays_in_month = 0;

    foreach ($holidays as $holiday) {
        // Don't count holidays that are not in the current month
        if (!str_contains($holiday, $yyyy_mm)) {
            continue;
        }

        // If holiday is on weekend, continue
        if (stringOrDateTimeToFormatted($holiday , 'N') > 5) {
            continue;
        }

        // Don't count holidays before today in current month if $onlyRemaining is true
        if ($onlyRemaining && strtotime($holiday) < strtotime($yyyy_mm_dd)) {
            continue;
        }

        // Do not calculate holiday on weekdays off
        if ((int) stringOrDateTimeToFormatted($holiday, 'N') === $day_off_weekday) {
            continue;
        }

        ++$holidays_in_month;
    }

    return $holidays_in_month;
}

function getWorkdaysRemainingThisMonth(): int {
    $weekdays = getWeekdaysRemainingThisMonth();
    $holidays = getHolidaysRemainingThisMonth();

    return $weekdays - $holidays;
}

function getWeekdaysInMonthOnDate(string | DateTime $date, $onlyRemaining = false): int {
    global $day_off_weekday;

    $d = stringOrDateTimeToFormatted($date, 'd');
    $m = stringOrDateTimeToFormatted($date, 'm');
    $y = stringOrDateTimeToFormatted($date, 'Y');

    $lastdayOfMonth = date("t", mktime(0, 0, 0, $m, 1, $y));
    $weekdays = 0;
    for ($day = $onlyRemaining ? $d : 1; $day <= $lastdayOfMonth; $day++) {
        $wd = date("N", mktime(0, 0, 0, $m, $day, $y));

        // If person has specific workday off do not add to working days
        if ((int)$wd === $day_off_weekday) {
            continue;
        }

        if ($wd > 0 && $wd < 6) {
            $weekdays++;
        }
    }

    return $weekdays;
}

function getWorkdaysInMonthOnDate(string | DateTime $date): int {
    $weekdays = getWeekdaysInMonthOnDate($date);
    $holidays = getHolidaysInMonthFromDate($date);

    return $weekdays - $holidays;
}

function humanReadable($time, $likeHarvest = true): string {
    if ($time === 0) {
        return $likeHarvest ? '0:00' : '0m';
    }
    $array = decimalTimeToHumanReadable($time);
    if ($likeHarvest) {
        return $array['hours'] .':'. ($array['minutes'] < 10 ? '0' : '') . $array['minutes'];
    }
    return ($array['hours'] > 0 ? $array['hours'] .'h ' : '') .$array['minutes'].'m';
}

function getExpectedHoursThisMonth(): float|int {
    global $working_hours_per_day;
    $workdays_in_month = getWorkdaysInMonthOnDate(new DateTime());
    return $workdays_in_month * $working_hours_per_day;
}

function getReportedHours(string|DateTime $from, string|DateTime|null $to = null): float|int
{
    global $user_id;

    if ($to === null) {
        $to = $from;
    }

    $from = stringOrDateTimeToFormatted($from);
    $to = stringOrDateTimeToFormatted($to);

    $report = queryHarvest('api/v2/time_entries?user_id=' . $user_id . '&from=' . $from . '&to=' . $to . '&per_page=500');

    $reported_hours = 0;

    foreach ($report->time_entries as $day) {
        $reported_hours += $day->hours;
    }

    return $reported_hours;
}


function getReportedHoursThisMonth(): float|int {
    return getReportedHours(new DateTime('first day of this month'), new DateTime('last day of this month'));
}

/**
 * @return bool
 */
function isTimerRunning(): bool
{
    global $user_id;
    $report = queryHarvest('api/v2/time_entries?user_id='.$user_id.'&is_running=true');
    return count($report->time_entries) !== 0;
}

/**
 * simple HTTP query wrapper
 *
 * @param string $method API URL segment to pass
 * @param string $post post data to be included
 *
 * @return object cURL response
 */

function queryHarvest(string $method, string $post = ''): object {
    global $bearer, $account_id;

    $url = 'https://api.harvestapp.com/'.$method;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Harvest-Account-ID: ' . $account_id,
        'Authorization: Bearer ' . $bearer,
        'User-Agent: Harvest API'
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_URL, $url);

    if (!empty($post)) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    }

    $response = curl_exec($ch);
    curl_close($ch);

    try {
        $result = json_decode($response, false, 512, JSON_THROW_ON_ERROR);
    } catch (JsonException $e) {
        die('Error: '.$e->getMessage());
    }

    return $result;
}

function isWorkingHours(): bool {
    $currentHour = date('G');

    if ($currentHour >= 8 && $currentHour < 17) {
        return true;
    }

    return false;
}
