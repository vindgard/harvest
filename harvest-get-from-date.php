<?php
/**
 * Gets all hours for a specific date
 */

require_once("config.inc.php");
require_once("functions.inc.php");

global $user_id;

$reported_time = 0.0;

$page = 1;
$date = readline('Enter startdate: ');
do {
    $report = queryHarvest('api/v2/time_entries?user_id='.$user_id.'&from='.$date.'&page='.$page);

    foreach ($report->time_entries as $key => $day) {
		# Sum hours
		echo str_pad($day->spent_date, 10)
		. ' | ' .str_pad($day->client->name, 20)
		. ' | ' . str_pad($day->project->name, 35) 
		. ' | ' . str_pad($day->hours, 8, ' ', STR_PAD_LEFT) 
		. ' | '
		. PHP_EOL;
		$reported_time += (double)$day->hours;
	}
	$page++;

} while ($report->links->next !== null);


echo PHP_EOL 
	. 'Total: ' 
	. $reported_time 
	. PHP_EOL;

$time_reported_human_readable = decimalTimeToHumanReadable($reported_time);

echo 'Human readable: ' 
	. $time_reported_human_readable['hours'] 
	. ' hours ' 
	. $time_reported_human_readable['minutes'] 
	. ' minutes' 
	. PHP_EOL; 
