<?php

require_once("config.inc.php");
require_once("functions.inc.php");

$report = queryHarvest('api/v2/time_entries?user_id=' . $user_id . '?is_running=true');

if (count($report->time_entries) !== 0) {
    echo '<div>'
        . '<i class="fa-solid fa-business-time"></i>'
        . '<span>'
        . $report->time_entries[0]->client->name
        . '</span>'
        . '</div>';
    return;
}

die('<div class="error"><i class="fa-solid fa-warning fa-fade"></i></div>');
