# Things

- [ ] Remove border on icons
- [ ] Use better icons for actions in top menu
- [ ] Make "reload projects from reports" non destructive or just not destroying your current config
- [ ] Improve UI to make things easier to read
- [ ] Remove unused property in config-json to make things easier to read
