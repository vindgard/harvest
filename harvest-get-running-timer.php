<?php

require_once("config.inc.php");
require_once("functions.inc.php");

global $hide_widget_on_weekends;
global $working_hours_per_day;
global $day_off_weekday;

# Create DateTime object from today
$today = new DateTime();

if ($hide_widget_on_weekends && isWeekendOnDate($today)) {
    //die('Enjoy!');
}


if (isTimerRunning()) {
	die('<div><i class="fa-solid fa-business-time"></i></div>');
}

die('<div class="error"><i class="fa-solid fa-warning fa-fade"></i></div>');
