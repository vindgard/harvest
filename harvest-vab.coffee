# This is a simple example Widget, written in CoffeeScript, to get you started
# with Übersicht. For the full documentation please visit:
#
# https://github.com/felixhageloh/uebersicht
#
# You can modify this widget as you see fit, or simply delete this file to
# remove it.

# this is the shell command that gets executed every time this widget refreshes
#command: "echo ''"
command: "/opt/homebrew/bin/php ~/git/personal/harvest/harvest-get-vab.php"

# the refresh frequency in milliseconds
refreshFrequency: 360000

# render gets called after the shell command has executed. The command's output
# is passed in as a string. Whatever it returns will get rendered as HTML.
render: (output) -> """
<div><pre>#{output}</pre></div>
"""

# the CSS style for this widget, written using Stylus
# (http://learnboost.github.io/stylus/)
style: """
  z-index: 10
  background: rgba(#000, 1)
  font-family: "Courier"
  bottom: 50%
  left: 40px;
  width: 500px;

  div
    color: rgba(#0f0, 1)
    font-size: 12px
    padding:0 20px 0 20px
    margin:0
  pre
    margin:0
    padding:0

"""