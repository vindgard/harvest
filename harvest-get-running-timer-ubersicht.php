<?php

require_once("config.inc.php");
require_once("functions.inc.php");
    
global $hide_widget_on_weekends;

# Create DateTime object from today
$today = new DateTime();

if ($hide_widget_on_weekends && isWeekendOnDate($today)) {
    die('');
}

if (isTimerRunning()) {
	die('<div class="timer timerRunning"></div>');
}
