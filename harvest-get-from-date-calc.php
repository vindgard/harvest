<?php
/**
 * Gets spent time AND calculates if you are ahead or behind the monthly total
 */


require_once("config.inc.php");
require_once("functions.inc.php");

$reported_time = 0.0;

$page = 1;
$date = readline('Enter startdate: ');
$days = readline('Working days:');
do {
    $report = queryHarvest('api/v2/time_entries?user_id=' . $user_id . '&from=' . $date . '&page=' . $page);

    foreach ($report->time_entries as $key => $day) {
        # Sum hours
        echo str_pad($day->spent_date, 10)
            . ' | ' . str_pad($day->client->name, 20)
            . ' | ' . str_pad($day->project->name, 35)
            . ' | ' . str_pad($day->hours, 8, ' ', STR_PAD_LEFT)
            . ' | '
            . PHP_EOL;

        $reported_time += (double)$day->hours;
    }
    $page++;
} while ($report->links->next !== null);


echo PHP_EOL . 'Needed: ' . $days * 8 . 'h' . PHP_EOL;
echo 'Total: '
    . humanReadable($reported_time, false) . PHP_EOL . PHP_EOL;

$time_reported_human_readable = decimalTimeToHumanReadable($reported_time);

$diff = $days * 8 - $reported_time;
if ($diff < 0) {
    echo 'You are ahead: ';
} else {
    echo 'You are behind: ';
}

echo humanReadable(abs($diff), false) . PHP_EOL;
