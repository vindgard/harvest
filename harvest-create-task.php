<?php

require_once("config.inc.php");
require_once("functions.inc.php");

# Get todays date
$today = date("Y-m-d");

$data = [
    'project_id' => 18431467,
    'task_id' => 7894279,
    'spent_date' => $today,
    'hours' => '0.2',
    'notes' => 'Torsdagsmöte',
];

$response = queryharvest('v2/time_entries?' . http_build_query($data), 'POST');

die(print_r($response));