<?php

date_default_timezone_set('Europe/Stockholm');

$bearer     = ''; // Get this from https://id.getharvest.com/developers
$account_id = ''; // Harvest generic account id
$user_id    = ''; // Login to Harvest, click my profile in the user menu and look in the url https://xxx.harvestapp.com/people/[your_user_id]/edit

$day_off_weekday = 0; // Leave at 0 for normal 5 working days
$workdays_per_week  = 5;
$working_hours_per_day = 6;
$colorize_hours_per_day_remaining = true; // adds faint green/red text color to the remaining hours per day count to indicate whether you are ahead or behind
$hide_widget_on_weekends = false; // if true, this widget will not be displayed on your desktop on weekends
$holidays = [
    '2023-04-07',
    '2023-04-10',
    '2023-05-01',
    '2023-05-18',
    '2023-05-28',
    '2023-06-06',
    '2023-06-24',
    '2023-11-04',
    '2023-12-25',
    '2023-12-26'
];
