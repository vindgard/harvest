<?php

require_once("config.inc.php");
require_once("functions.inc.php");

$date = (new DateTimeImmutable())->sub(new DateInterval('P90D'));
$fskDeadLineDate = $date->format('Y-m-d');

$output = "

____   _________ __________.__               __                
\   \ /   /  _  /\\______   \__| ____ _____ _/  |_  ___________ 
 \   Y   /  /_\  \|    |  _/  |/    \/\__  /\\  __\/  _ \_  __ \
  \     /    |    \    |   \  |   |  \/ __ \|  | (  <_> )  | \/
   \___/\____|__  /______  /__|___|  (____  /__|  \____/|__|   
                \/       \/        \/     \/                   
                            All your illness are belong to FSK


       =================================================
       |   Date     | Hours |      Contaminant         |
       +------------+-------+--------------------------+
";
$reported_time = 0.0;

$page = 1;

do {
    $report = queryHarvest(
        'api/v2/time_entries?project_id=14814226&task_id=18851933&user_id=' . $user_id . '&from=' . $fskDeadLineDate . '&page=' . $page
    );

    foreach ($report->time_entries as $key => $day) {
        # Make sure notes is not null
        $notes = $day->notes ?? '';

        # if sting containst [fsk] then it has been reported
        if (strpos($notes, '[fsk]') !== false) {
            continue;
        }

        $output .= '       | '
            . str_pad($day->spent_date, 10)
            . ' | '
            ##. str_pad($day->task->name, 10)
            ##. ' | '
            . str_pad($day->hours, 5, ' ', STR_PAD_LEFT)
            . ' | '
            . str_pad($notes, 24)
            . ' | '
            . PHP_EOL;
        $reported_time += (double)$day->hours;
    }
    $page++;
} while ($report->links->next !== null);

$time_reported_human_readable = decimalTimeToHumanReadable($reported_time);

$output .= '       +-----------------------------------------------+
       | ' . str_pad(
        $time_reported_human_readable['hours'] . ' hours ' . $time_reported_human_readable['minutes'] . ' minutes',
        45,
        ' ',
        STR_PAD_BOTH
    ) . ' |
       =================================================' . PHP_EOL . PHP_EOL;


if ($reported_time > 0) {
    echo $output;
}