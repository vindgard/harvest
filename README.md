# Harvest Ubersicht widget

The repo contains three different Ubersincht widgets. One that shows hours and minutes reported today and one that shows reported time per day for the whole week and a widget for showing hours that needs to be reported to Försäkringskassan.

## Installation

1. Install Ubersicht
2. Clone the repo
3. Symlink the `coffee` files you want to your widget dir to keep them versioned
    - `ln /path/to/your/repo/file.coffee /path/to/your/widgets/folder/file-symlinked.coffee`
4. Edit the `command:"` line to match where your repo is residing and where you have your php binary
5. Create a copy of `config.inc.rename` and rename to `config.inc.php` and update Harvest credentials and timezone if needed.
6. Hack the `harvest-today.coffee`, `harvest-vab.coffee`  and `harvest-week.coffee` to suit your style
7. Add new nice features!
8. There is no step 8

## The three different widgets

Each widget has one php file accompanying each widget.

- `harvest-get-week.php` is used by `harvest-week.coffee`, shows reported time for current week
- `harvest-get-today.php` is used by `harvest-today.coffee`, shows reported time for current day
- `harvest-get-vab.php` is used by `harvest-vab.coffee`, shows any unreported hours for fsk so that you dont forget to

## Get from date

Run `php harvest-get-from-date.php` and it will ask for a start date. This will get the total from that date

## VAB BONUS

After configuring everything, check if you have any unreported hours at FSK by runnig `/opt/homebrew/bin/php harvest-get-vab.php`. Any reported task with `[fsk]` is interpered as reported and is not shown. Anything else is shown, but only **90** days back since that is the FSK deadline. Ask me how I know.
