<?php

require_once("config.inc.php");
require_once("functions.inc.php");

global $hide_widget_on_weekends;
global $working_hours_per_day;
global $day_off_weekday;

## Get config options
$options = getopt('', [
    'showWidgetAfterWorkingHours::',
    'showTimerRunningIndicator::',
]);

$always_show_widget = array_key_exists('showWidgetAfterWorkingHours', $options) && $options['showWidgetAfterWorkingHours'] === 'true';
$show_timer_running = array_key_exists('showTimerRunningIndicator', $options) && $options['showTimerRunningIndicator'] === 'true';

# Do not show widget
if (!$always_show_widget && !isWorkingHours()) {
    die('');
}

# Create DateTime object from today
$today = new DateTime();

if ($hide_widget_on_weekends && isWeekendOnDate($today)) {
    die('');
}

# Get numeric day of week. 1-7
$day_of_week = $today->format("N");

# Get last monday, what happens on mondays?
# FIXME: There must be a better and nicer way to solve this
if ($day_of_week === "1") {
    # If day is Monday use today
    $datetime = new DateTime();
} else {
    # If day is not Monday, use "last Monday"
    $datetime = new DateTime("last Monday");
}

$reported_hours_today = getReportedHours($today->format('Y-m-d'), $today->format('Y-m-d'));
$expected_hours = getExpectedHoursThisMonth();
$reported_hours = getReportedHoursThisMonth() ;

$hours_remaining = $expected_hours - $reported_hours;
$days_remaining = getWorkdaysRemainingThisMonth();
$hours_remaining_per_day = $hours_remaining / $days_remaining;

$current_workday_is_over = (new DateTime())->format('H') > 18 || $reported_hours_today >= $hours_remaining_per_day;

$days_remaining_after_today = $current_workday_is_over && $days_remaining > 0 ? $days_remaining - 1 : $days_remaining;

$hours_remaining_per_day_adjusted = $days_remaining_after_today > 0 ? $hours_remaining / $days_remaining_after_today : $hours_remaining;


// If workday is over, use 100 otherwise it will recalculate based on remaining hours and we want to celebrate a full days work!
if ($current_workday_is_over) {
    $percentage_completed_today_100_max = 100;
} else {
    $percentage_completed_today = floor(($reported_hours_today / $hours_remaining_per_day_adjusted) * 100);
    $percentage_completed_today_100_max = min($percentage_completed_today, 100);
}


$is_timer_running = isTimerRunning();

$reported_hours_this_week = 0;

$timer_running_class = ($is_timer_running && $show_timer_running) ? 'timer-running' : '';

# Start output
echo '<div class="weekdays ' . $timer_running_class . '">';
$i = 0;
while ($i < 5) {
    # Iterate counter
    $i++;

    # Check if we want to continue (mon-fri)
    // TODO: $i is never 0 because counter gets incremented immediately above, so the following is never executed
    if ($i === 0 && $datetime->format('N') === '7') {
        $datetime->add(new DateInterval('P1D'));
        continue;
    }

    # Make sure weekdays off are respected and not shown
    if ($i === $day_off_weekday) {
        continue;
    }

    # Get current date
    $curr_date = $datetime->format('Y-m-d');
    $isToday = $today->format('Y-m-d') === $curr_date;

    $today_class = $isToday ? ' today' : '';
    $timer_class = ($is_timer_running && $isToday) ? ' timer-running' : '';
    $holiday_class = isHolidayOnDate($curr_date) ? ' holiday' : ' no-holiday';

    # Create today's cell and add class if date is today
    echo '<div class="weekday'.$today_class.$timer_class.$holiday_class.'">';

    $holiday_icon = '*';
    $holiday_indicator = isHolidayOnDate($curr_date) ? '<span>'.$holiday_icon.'</span>' : '';

    # Echo date
    #echo '<div class="date">' . substr($datetime->format('D'), 0, 1) . $holiday_indicator . '<small>'.$datetime->format('j/n').'</small></div>';

    $hours_today = getReportedHours($curr_date, $curr_date);
    $formatted_hours_today = $hours_today > 0 ? humanReadable($hours_today) : humanReadable(0);

    $reported_hours_this_week += $hours_today;


    # Echo reported time
    echo '<div class="hours">'.$formatted_hours_today.'</div>';

    if ($today->format('Y-m-d') === $curr_date) {
        echo '<div class="highlight" style="width: ' . $percentage_completed_today_100_max . '%"></div>';
    }

    echo "</div>";

    # Get next day
    $datetime->add(new DateInterval('P1D'));
}
echo '</div>';
echo '<div class="weekly-summary">' . $reported_hours_this_week . '</div>';
