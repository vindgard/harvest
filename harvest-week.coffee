# This is a simple example Widget, written in CoffeeScript, to get you started
# with Übersicht. For the full documentation please visit:
#
# https://github.com/felixhageloh/uebersicht
#
# You can modify this widget as you see fit, or simply delete this file to
# remove it.

# this is the shell command that gets executed every time this widget refreshes
# --showWidgetAfterWorkingHours='true' 
# default is false, this means that the hours reported only shows between 8-17
# --showTimerRunningIndicator='true'
# default is false

#command: "/opt/homebrew/bin/php ~/git/personal/harvest/harvest-get-week-slim.php --showWidgetAfterWorkingHours='true'"
#command: "/opt/homebrew/bin/php ~/git/personal/harvest/harvest-get-week-slim.php --showTimerRunningIndicator='true'"
command: "/opt/homebrew/bin/php ~/git/personal/harvest/harvest-get-week-slim.php --showTimerRunningIndicator='true' --showWidgetAfterWorkingHours='true'"
#command: "/opt/homebrew/bin/php ~/git/personal/harvest/harvest-get-week-slim.php"

# the refresh frequency in milliseconds
refreshFrequency: 36000

# render gets called after the shell command has executed. The command's output
# is passed in as a string. Whatever it returns will get rendered as HTML.
render: (output) -> """
<div class="wrapper">#{output}</div>
"""

# the CSS style for this widget, written using Stylus
# (http://learnboost.github.io/stylus/)
style: """
  z-index: 10
  background: rgba(#000, 0)
  font-family: "Helvetica Neue"
  top: 7px
  left: 56.5%
  width: 600px
  font-weight: lighter

  div.weekday
    width: 60px
    overflow: hidden
    position: relative
  div.weekday:first-child
    border-top-left-radius: 6px
    border-bottom-left-radius: 6px
  div.weekday:last-child
    border-top-right-radius: 6px
    border-bottom-right-radius: 6px
  div.wrapper
    position: relative
    display: flex
    color: rgba(#fff, 0.7)
    font-size: 15px
  .weekdays.timer-running::before
    content: ""
    width: 5px
    margin: 10px 5px
    border: 1px solid rgba(#fff,0.2)
    border-radius: 5px
  .weekdays
    display: flex
  small
    font-size: 13px
    letter-spacing: 1px;
    line-height: 13px;
  .date
  .remaining
    opacity: 0.75
    display: flex
    align-items: center
    justify-content: space-between
    background: rgba(#fff,0.2)
    color: rgba(#fff, 0.9)
    padding: 6px 16px
    font-weight: 500
    text-transform: uppercase;
    font-size: 13px
    letter-spacing: 1px;
    height: 18px;
    line-height: 13px;
  .hours,
  .hours-per-day,
  .weekly-summary
    height: 28px
    display: flex
    align-items: center
    justify-content: center
    background: rgba(#fff,0.1)
    font-size: 16px
    font-weight: lighter
    text-align: center
  .weekly-summary
    margin-left: 10px
    padding: 0 10px
    border-radius: 6px
  .weekly-summary::before
    display: block
    position: relative
    width: 10px
    left: -10px
    height: 3px
    content: ''
    margin-left: -10px
    background-color: rgba(#fff, 0.1)
  .today .hours
  .today .date
    color: rgba(#fff, 1)
  .today .date
    background: rgba(#fff,0.3)
  .today .hours
    background: rgba(#fff,0.2)
  .highlight
    left: 0
    height: 2px
    position: absolute
    bottom: 0
    background-color: rgba(#fff, 0.4)
    box-shadow: 0 0 20px 0 rgba(#fff, 0.2)
  .today.timer-running .highlight
    animation-name: pulse
    animation-duration: 2s
    animation-iteration-count: infinite
    animation-direction: alternate
    animation-timing-function: ease-in-out

  .alert
    color: rgba(200,0,0, 0.5)
    letter-spacing: 1px
    text-transform:uppercase
  .remaining-this-month
    margin-left: 16px;
    border-top-left-radius: 16px
    border-top-right-radius: 16px
    overflow: hidden
    flex: 1 0 186px;
    display: flex
    flex-wrap: wrap;
  .remaining-this-month .remaining
    justify-content: center
    flex: 1 0 50%;
    text-align: center;
  .remaining-this-month .hours,
  .remaining-this-month .hours-per-day
    height: 56px
    flex-wrap: wrap
    flex: 1 0 50%;
  .remaining-this-month .hours
    border-bottom-left-radius: 16px
  .remaining-this-month .hours-per-day
    border-bottom-right-radius: 16px
  .remaining-this-month .hours.awesome,
  .remaining-this-month .hours-per-day.awesome
    color: palegreen;
    line-height: 1;
  .remaining-this-month .hours.awesome .days-remaining
    color: rgba(palegreen, 1)
  .remaining-this-month .hours .days-remaining
  .remaining-this-month .hours-per-day .days-remaining
    font-size: 12px
    display: block
    width: 100%
    margin-top: -8px
    letter-spacing: 0.5px
    opacity: 0.8

    @keyframes pulse
      0%
        background-color: rgba(#fff, 0.4)
        box-shadow: 0 0 10px 0 rgba(#fff, 0.2)
      100%
        background-color: rgba(#fff, 1)
        box-shadow: 0 0 20px 0 rgba(#fff, 0.5)
"""
